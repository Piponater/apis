import base64
import requests
import json

f = open("coke.jpg", "rb")
payload = f.read()


url = "https://microsoft-azure-microsoft-computer-vision-v1.p.rapidapi.com/analyze"

querystring = {"visualfeatures":"Categories,Tags,Color,Faces,Description"}

headers = {
    'x-rapidapi-host': "microsoft-azure-microsoft-computer-vision-v1.p.rapidapi.com",
    'x-rapidapi-key': "",
    'content-type': "application/octet-stream"
    }

response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

print(response.text)

data = response.json()
print(json.dumps(data, indent=1))
